const database = require('../model/Database')
const Item = require('../model/task')
const db = new database('list.db')

describe('Item class', () => {
    test('has getTasks method which return promise', () => {
        const item = new Item(db)
        let spy = jest.spyOn(item, 'getTasks').mockImplementation(() => Promise.resolve());
        expect(typeof item.getTasks).toBe('function')
        expect(item.getTasks()).toEqual(Promise.resolve())
    })
    test('has addTasks method which return promise', () => {
        const item = new Item(db)
        let spy = jest.spyOn(item, 'addTasks').mockImplementation(() => Promise.resolve());
        expect(typeof item.addTasks).toBe('function')
        expect(typeof item.addTasks('test')).toBe('object')
        expect(item.addTasks()).toEqual(Promise.resolve())
    })
    test('has updateRank method which return promise', () => {
        const item = new Item(db)
        let spy = jest.spyOn(item, 'updateRank').mockImplementation(() => Promise.resolve());
        expect(typeof item.updateRank).toBe('function')
        expect(typeof item.updateRank('test')).toBe('object')
        expect(item.updateRank()).toEqual(Promise.resolve())
    })
    test('has deleteTask method which return promise', () => {
        const item = new Item(db)
        let spy = jest.spyOn(item, 'deleteTask').mockImplementation(() => Promise.resolve());
        expect(typeof item.deleteTask).toBe('function')
        expect(typeof item.deleteTask('test')).toBe('object')
        expect(item.deleteTask()).toEqual(Promise.resolve())
    })
    afterAll(() => {
        jest.restoreAllMocks();
    })
})