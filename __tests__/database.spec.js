const database = require('../model/Database')
const Item = require('../model/database')
const db = new database('list.db')

describe('Item class', () => {
    test('has connect method which return promise', () => {
        const item = new Item(db)
        let spy = jest.spyOn(item, 'connect').mockImplementation(() => Promise.resolve());
        expect(typeof item.connect).toBe('function')
        expect(item.connect()).toEqual(Promise.resolve())
    })
    afterAll(() => {
        jest.restoreAllMocks();
    })
})