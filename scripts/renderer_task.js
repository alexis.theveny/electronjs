window.addEventListener('DOMContentLoaded', async () => {
    const form = document.querySelector('form')
    const el = document.querySelector('#input')
    let task

    form.addEventListener('submit', submitForm)
    function submitForm(e) {
      e.preventDefault();
      addTask(el.value)
    }

    function addTask(data) {
        if (data) {
          window.task.send("task:add", data)
        } else {
          console.log('Data obligatoire');
        }
      }
})