const { contextBridge, ipcRenderer } = require('electron')


contextBridge.exposeInMainWorld(
    "board", {
        send: (channel, data = null) => {
            ipcRenderer.send(channel, data)
        },
        receive: (channel, func) => {
            ipcRenderer.on(channel, (e, ...args) => func(...args))
        }
    }
)