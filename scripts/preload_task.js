const { contextBridge, ipcRenderer } = require('electron')

contextBridge.exposeInMainWorld(
    "task", {
        send: (channel, data = null) => {
            ipcRenderer.send(channel, data)
        },
        getTask: (channel, func) => {
            ipcRenderer.on(channel, (e, ...args) => func(...args))
        }
    }
)