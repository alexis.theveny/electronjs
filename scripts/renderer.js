window.addEventListener('DOMContentLoaded', async () => {
    document.getElementById('new-board').addEventListener("click", () => {
        window.board.send('board:addTask', 'Add Task')
    }, false)

    // Affichage des boards
    window.board.send('board:read')
    window.board.receive('async:board:read', (data) => {
        const ul = document.getElementById('boards')
        for (let i = 0; i < data.length; i++) {
            ul.innerHTML += (`<ul class='container mx-2 message' id='` + data[i].id + `'>
                                    <h1 class='message-header' id='title`+ data[i].id + `'>` + data[i].title + `</h1>
                                </ul>`)

            // const li = document.createElement('li')
            // li.innerText = data[i].title
            // ul.appendChild(li)
        }
    })

    let boardCount = [0,0,0,0]

    // Affichage des tasks
    window.board.send('task:read')
    window.board.receive('async:task:read', (data) => {
        let sizeArray = 0;
        data.forEach((task, index, array) => {
            let board = document.getElementById(task.id_List.toString())
            countBoard(task.id_List.toString())
            board.innerHTML += (`<li class='box m-2 draggable notification is-link is-light' draggable='true' id="task:` + task.id + `">
                                        `+ task.title + `
                                    <input type="button" class="btn_supp button is-danger is-small" id="task_del:` + task.id + `" value="Supp"/>
                                    </li>`)


            sizeArray++;
            if (sizeArray === array.length) {
                init();
            }
        });
        for(let i=0; i<4; i++){
            document.getElementById(`title${i+1}`).innerHTML += ("<small>(" + boardCount[i] + ")</small>")
            if (boardCount[i] == 0) {
                document.getElementById(`${i+1}`).innerHTML += "<span class='m-2'>Aucune task</span>"
            }

        }
        })


    function countBoard(id_List) {
        switch (id_List) {
            case '1':
                boardCount[0]++
                break;
            case '2':
                boardCount[1]++
                break;
            case '3':
                boardCount[2]++
                break;
            case '4':
                boardCount[3]++
                break;
            default:
                break;
        }
    }

    init = () => {

        b = document.querySelectorAll('.btn_supp')
        b.forEach(element => {
            element.addEventListener('click', e => {
                e.preventDefault()
                console.log(e.srcElement.id.replace('task_del:', '') * 1)
                window.board.send("board:delete", (e.srcElement.id.replace('task_del:', '') * 1))
            })
        })
        
        const draggables = document.querySelectorAll('.draggable')
        const containers = document.querySelectorAll('.container')

        draggables.forEach(draggable => {
            draggable.addEventListener('dragstart', () => {
                draggable.classList.add('dragging')
            })

            draggable.addEventListener('dragend', () => {
                draggable.classList.remove('dragging')
            })
        })

        containers.forEach(container => {
            container.addEventListener('dragover', e => {
                e.preventDefault()
                const afterElement = getDragAfterElement(container, e.clientY)
                const draggable = document.querySelector('.dragging')
                if (afterElement == null) {
                    container.appendChild(draggable)
                } else {
                    container.insertBefore(draggable, afterElement)
                }
            })

            //Ecouter l'item dropper
            container.addEventListener('drop', e => {
                e.preventDefault()

                //id du container de fin
                //console.log("container id: "+e.target.offsetParent.id)

                //retrouver la position dans la colonne

                /*position = Array.from(
                    document.querySelector('.dragging').parentElement.children
                  ).indexOf(document.querySelector('.dragging'))
                console.log("drop position: "+position)*/

                //toute les autres positions des items
                const childReview = document.getElementById(e.target.offsetParent.id);
                let data = []
                for (let i = 1; i < childReview.children.length; i++) {
                    data.push(childReview.children[i].id.replace('task:', '') * 1)
                }
                //console.log([e.target.offsetParent.id * 1, data])

                window.board.send('board:dropUpdate', [e.target.offsetParent.id * 1, data])
            })
        })

        function getDragAfterElement(container, y) {
            const draggableElements = [...container.querySelectorAll('.draggable:not(.dragging)')]

            return draggableElements.reduce((closest, child) => {
                const box = child.getBoundingClientRect()
                const offset = y - box.top - box.height / 2
                if (offset < 0 && offset > closest.offset) {
                    return { offset: offset, element: child }
                } else {
                    return closest
                }
            }, { offset: Number.NEGATIVE_INFINITY }).element
        }
    }


})