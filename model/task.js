class Task {
    constructor(db) {
      this.db = db.connect()
    }
    getTasks() {
      return new Promise((resolve, reject) => {
        this.db.all("SELECT * FROM Task ORDER BY rank", (err,rows) =>{
          if(err) {
            console.log(err);
            reject(err)
          } else {
            resolve(rows)
          }
        })
      });
    }
    addTasks(data) {
      return new Promise((resolve, reject) => {
        const stmt = this.db.prepare("INSERT INTO Task (title, rank, id_List) VALUES (?, 1, 1)")
        stmt.run(data, (err,rows) => {
          if(err) {
            reject(err)
          } else {
            resolve(rows)
          }
        })
      })
    }
    updateRank(taskId, listId, rank){
      return new Promise((resolve, reject) => {
        const stmt = this.db.prepare("UPDATE Task SET id_List=?, rank=? WHERE id=?")
        stmt.run(listId, rank, taskId, (err,rows) => {
          if(err) {
            reject(err)
          } else {
            resolve(rows)
          }
        })
      })
    }
    deleteTask(data) {
      return new Promise((resolve, reject) => {
        const stmt = this.db.prepare("DELETE FROM Task WHERE id = ?")
        stmt.get(data, (err,rows) => {
          if(err) {
            reject(err)
          } else {
            resolve(rows)
          }
        })
      })
    }
  }
  
  module.exports = Task