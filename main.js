const { app, BrowserWindow, Menu, ipcMain, Notification, dialog } = require('electron')
const path = require('path')

// database
const database = require('./model/database.js')
const Task = require('./model/task.js')
const List = require('./model/list.js')
const db = new database('list.db')
const tasks = new Task(db)
const lists = new List(db)

// Menu
const menu = [
    {
        label: 'File',
        submenu: [
            // {
            //     label: 'New item',
            //     click() {
            //         if (win) {
            //             win.show()
            //         } else {
            //             createNewWindow()
            //         }
            //     },
            // },
        ],

    },
    {
        label: 'View',
        submenu: [
            {
                label: 'Toggle DevTools',
                accelerator: process.platform === 'darwin' ? 'Command+I' : 'Ctrl+I',

                click(item, focusedWindow) {
                    focusedWindow.webContents.toggleDevTools()
                }
            },
            {
                role: 'reload'
            }
        ]
    },
    {
        label: 'Quit',
        accelerator: 'CommandOrControl+Q',
        click() {
            app.quit()
        }
    }
]

// Creation de fenetre
const createWindow = () => {
    const win = new BrowserWindow({
        width:1080,
        height:720,
        webPreferences: {
            preload: path.join(app.getAppPath(), 'scripts/preload.js'),
            nodeIntegration: false,
            contextIsolation: true,
            enableRemoteModule: false
        }
    })

    win.loadFile('views/index.html')
    return win
}

// Creation fenetre form

const createAddBoardWindow = () => {
    const win = new BrowserWindow({
        width: 500,
        height: 400,
        webPreferences: {
            preload: path.join(app.getAppPath(), 'scripts/preload_task.js'),
            nodeIntegration: false,
            contextIsolation: true,
            enableRemoteModule: false
        }
    })

    win.loadFile('views/task.html')
    // return win
}

// Ouvrir la fenetre quand l'app est prete
let win
app.whenReady().then(() => {
    win = createWindow()
    win.maximize()

    app.on('activate', () => {
        if (BrowserWindow.getAllWindows().length === 0) createWindow()
    })
    const mainMenu = Menu.buildFromTemplate(menu)
    Menu.setApplicationMenu(mainMenu)
})

// Quitter completement l'app a la fermeture de page
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') app.quit()
})

// let formWin
ipcMain.on('board:addTask', (e, data) => {
    console.log(data)
    // formWin = createAddBoardWindow()
    createAddBoardWindow()
})

ipcMain.on('board:read', (e, data) => {
    lists.getLists().then(
        data => {
            win.webContents.send('async:board:read', data)
        }
    )
})

ipcMain.on('task:read', (e, data) => {
    tasks.getTasks().then(
        data => {
            win.webContents.send('async:task:read', data)
        }
    )
})

ipcMain.on('board:dropUpdate', (e, data) => {
    // array[0] = id colonne
    // array[1] = data position avec id
    const notif = new Notification({
        title: 'Task changée',
        body: 'Bravo vous avez changé une task'
    })
    console.log(data)
    let listId = data[0]
    let rank = 0
    data[1].forEach(element => {
        rank = rank + 1
        console.log(rank + " " + element +" "+ listId)
        tasks.updateRank(element, listId, rank)
            .then(
                () => {
                    notif.show()
                    win.reload()
                }
            )
        });
})

ipcMain.on('task:add', (e, data) => {
    const notif = new Notification({
        title: 'Task ajouté',
        body: 'Bravo vous avez ajouté une task'
    })
    tasks.addTasks(data)
        .then(
            () => {
                //win.webContents.send('async:task:read', data)
                // formWin.quit()
                win.reload()
            },
            error => console.log(error)
        )

    notif.show()
})

ipcMain.on('board:delete', (e, data) => {
    console.log(data + "dans le main")
    dialog.showMessageBox({
        type: 'warning',
        title: 'Attention !',
        message: 'Faites très attention. C\'est très dangereux de supp (djeun\'s) une tache.',
        buttons: ['Supp quand même', 'J\'avoue en fait nan']
    })
    .then(res => {
        if(res.response == 0){
            tasks.deleteTask(data)
                .then(
                    () => {
                        //win.webContents.send('async:task:read', data)
                        // formWin.quit()
                        win.reload()
                    },
                    error => console.log(error)
                )
            }
        })
})